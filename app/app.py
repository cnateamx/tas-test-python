import mysql.connector
import os
import redis
import pika, sys
from flask import Flask, render_template

app = Flask(__name__)
RETRIEVE_SQL = 'SELECT * FROM my_users'
bgcolor = os.environ.get('APP_COLOR') or "green"

@app.route("/")
def home():
    # mySQL test
    mydb = mysql.connector.connect(
        host="9f63c8de-e350-496a-b9ab-c358c3a62c33.mysql.service.internal",
        user="41aa2d578d1d445ab85a6361bc7636e3",
        password="ww7tw1bdzwmr4rqw",
        db="service_instance_db"
    )

    mycursor = mydb.cursor(dictionary=True)
    mycursor.execute('SELECT * FROM my_users')
    myresult = mycursor.fetchone()
    name = myresult['firstname'] + ' ' + myresult['lastname']
    mycursor.close();
    mydb.close();

    # Redis test
    r = redis.Redis(
        host='q-s0.redis-instance.services.service-instance-f63eab8e-cd36-4626-bec5-8727e68fa92b.bosh',
        port=6379, 
        password='luSMyQmd3RAZBbnxtqWqokhZ0AHoe0')

    r.set('firstname', 'peter')
    r.set('lastname', 'redis')

    firstname = r.get('firstname').decode('UTF-8')
    lastname = r.get('lastname').decode('UTF-8')

    redisResult = firstname +  " " + lastname

    return render_template('index.html', bgcolor=bgcolor, myresult=name, redisResult=redisResult)

@app.route("/send/<message>")
def send(message):
    connection = pika.BlockingConnection(
    pika.URLParameters('amqp://eb6f7ceb-8b85-4fe8-919b-3b362cc9c4c6:i1e__sLqu3jBM9njD6GuLemt@q-s0.rabbitmq-server.services.service-instance-afcefb2f-775f-417e-b0ff-edb9ba98109f.bosh/afcefb2f-775f-417e-b0ff-edb9ba98109f'))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    channel.basic_publish(exchange='', routing_key='hello', body=str(message))
    connection.close()

    return render_template('index.html', bgcolor=bgcolor)

@app.route("/receive")
def receive():
    connection = pika.BlockingConnection(pika.URLParameters('amqp://eb6f7ceb-8b85-4fe8-919b-3b362cc9c4c6:i1e__sLqu3jBM9njD6GuLemt@q-s0.rabbitmq-server.services.service-instance-afcefb2f-775f-417e-b0ff-edb9ba98109f.bosh/afcefb2f-775f-417e-b0ff-edb9ba98109f'))

    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

    return render_template('index.html', bgcolor=bgcolor)

@app.route("/createTable")
def createTable():
    mydb = mysql.connector.connect(
        host="9f63c8de-e350-496a-b9ab-c358c3a62c33.mysql.service.internal",
        user="41aa2d578d1d445ab85a6361bc7636e3",
        password="ww7tw1bdzwmr4rqw",
        db="service_instance_db"
    )

    mycursor = mydb.cursor(dictionary=True)
    mycursor.execute("CREATE TABLE IF NOT EXISTS my_users (firstname VARCHAR(255), lastname VARCHAR(255))")
    mycursor.close();
    mydb.close();

    return render_template('index.html', bgcolor=bgcolor)

@app.route("/insertRecord")
def insertRecord():
    mydb = mysql.connector.connect(
        host="9f63c8de-e350-496a-b9ab-c358c3a62c33.mysql.service.internal",
        user="41aa2d578d1d445ab85a6361bc7636e3",
        password="ww7tw1bdzwmr4rqw",
        db="service_instance_db"
    )

    mycursor = mydb.cursor(dictionary=True)
    mycursor.execute("INSERT IGNORE INTO my_users (firstname, lastname) VALUES('peter', 'gosling');")
    mydb.commit()
    mycursor.close();
    mydb.close();

    return render_template('index.html', bgcolor=bgcolor)

if __name__ == '__main__':
    app.run(port=8080, host="0.0.0.0")
